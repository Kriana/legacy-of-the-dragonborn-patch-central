Scriptname DBM_IcebladeAliasFilter Extends ReferenceAlias

FormList Property IceBladeList Auto
GlobalVariable Property DBM_AMrelic04 Auto

Event OnInit()
	AddInventoryEventFilter(IcebladeList)
EndEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	if akBaseItem == IcebladeList
		if GetOwningQuest().GetStage() == 10
			GetOwningQuest().Setstage(30)
			DBM_AMrelic04.Value = 1
		Else
			DBM_AMrelic04.Value = 1
		Endif
	Endif
EndEvent