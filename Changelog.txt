6.8.0

Changed version number for consistency with main Legacy mod.
Lunar weapon Replacer: Updated for latest version of that mod.
Oblivion Artifact Pack: Adjusted Frostwyrm's activator position to allow eaiser manual placement/removal.
Serenity - A Silent Moons Camp Overhaul: Updated to legacy's new models for both Prefab and IKEA versions.

6.7.4

Fixed a small alignment issue with the 10th anniversary GCoN mod. Otherwise no changes were necessary. This patch is still usable on the old version, but the pyramid might look funny.
Removed some no longer used recipes from CCOR patch.
Fixed Jel's camp with Enhanced Landscapes.
Fixed the fomod which somehow broke sometime before the last update and no one noticed... until now.

6.7.3

Navmesh fixes Redux for these plugins (Czasior): Drengin's Blue Palace Terrace, JK's Blue Palace, Redbag's Solitude.

Gray Cowl of Nocturnal: Fixed some capitalization/punctuation.
Zim's Immersive Artifacts: Museum replicas can now be made from Zim's alternate relics.

*** NOTICE: There is no longer a separate install choice for Zim's Thane Weapons plugin.
Legacy's Display Patch for Zim's Thane Weapons installs with the COMPLETE Zim's Immersive Weapons install option ONLY!***

6.7.2

Added Lepidoptera

6.7.1

The following plugins needed navmeshes fixed due to a change in Legacy v6.7.0:
Drengin's Blue Palace Terrace
Dawn of Skyrim
JK's Skyrim/Dawn of Skyrim Combo
JK's Skyrim/Dawn of Skyrim/Deadly Shadows Combo
Redbag's Solitude

6.7.0

Aetherium Armor and Weapons: Removed an unnecessary injected record from the plugin
Drengin's Blue Palace Terrace: Updated patch for version 2.0.
Dwemer Pipework Reworked: Corrected mesh support for the newest version of this mod. 
Falskaar: Replaced incorrect dynamic display script on 2 items. Added Vanvir's Masterpiece to cheat crate.
Heavy Armory: Added displays for a few items that were missing.
JS Daggers: The JS model will now show for the Blade of Sacrifice correctly.
JS Rings: Added missing JS models for Calcemo's Ring and Ring of the Moon replicas.
Konahriks Accoutrements: Removed two unnecessary injected records from the plugin.
Morrowloot Ultimate: Cleaned up a persistent world record.
Requiem: Corrected a clipping note in Mercer Frey's House.
SUDs: Removed a stray case clipping into a wall nearby that isn't needed.
Tools of Kagrenec: Incorporated script fixes to play nice with Auryen's Journal.
Wheels of Lull: Removed duplicate textures from the BSA.
Zim's Immersive Artifacts: Thane Weapons plugin will now install correctly.

6.6.1

Drengin's Blue Palace Terrace: Fixed clipping rock on the Explorer's Guild balcony.
Helgen Reborn: Corrected Dragon's Fang activator name.
Reliquary of Myth: Thief Note now correctly indicates the updated location of the "item".

6.6.0

Advanced Adversary Encounters: Updated Bash Tags.
Clockwork: Added missing dig site activator in Nurndural - Mortal Layer.
Cutting Room Floor: This mod no longer places the Ring of Khajiit, so those changes have been removed from the patch.
Enhanced Solitude: Navmesh work around the Museum gate.
Morrowloot Ultimate: Serious update, many changes/fixes.
Requiem: Updated to add the new Lunar Steel Weapon models.
Sands of Time: Bash Tag update and small Levelled List record fix.
Weapons Armor Clothing Clutter Fixes: Updated new Lunar weapons textures in patch.
YASH: Serious update, many changes/fixes.
ADDED - Civil War Overhaul Redux: Fixes the Model/Texture issue for the Needle weapon.

6.5.0

Books Books Books: Removed 4 books that Legacy already covers.
BUVARP: The patch has been changed to use the new version of this mod.
Drengin's Blue Palace Terrace: Included edited Solitude base LOD mesh that was missing.
Heavy Armory: Added the missing Forsworn Hatchet to display.
Oblivion Artifact Pack: Adjusted the activator locations of a few of the armors to allow manual removal.
Requiem: Updated for Legacy v6+.

6.3.1

ADDED: Drengin's Blue Palace Terrace patch (leontristain, Jonado, Czasior)
Artificer - Artifacts of Skyrim: Removed an incorrect plugin from the install folder (delete DBM_Artificer_Patch)
Enhanced Solitude: Returned Bryling's house from the Abyss and put Astius' door down there.
Redbags Solitude: Corrected some Occlusion Pane issues (Czasior)


6.3.0

Books Books Books: Updated Bash Tags.	
Deadly Shadows: Fixed up issues with the JK's Skyrim and Dawn of Skyrim synergy patches.	
Enhanced Solitude: Killed Astius and demolished their house.	
Hearthfire Dolls: Re-fixed again.	
Helgen Reborn: Carried forward a setting from main ESM to Airship Cabin Cell that was missing.	
JK's Skyrim: Made sure the supply crate had proper script in the various versions of JK Patches.	
Redbag's Solitude: Removed an obnoxious collision box.	
RS Children: Tweaked Verna's appearance and regenerated Facegen files.	
ADDED: Immersive Fort Dawnguard patch (courtesy of Jonado1)

6.2.1

Artifacts of Boethiah: Fixed a conflict between Lysirius' and Alessandra's dagger. 
{May require a new game to see the change, as they're persistent.)


6.2.0

Dawn of Skyrim: Corrected Navmesh issues and item clipping.	
Redbag's Solitude: Corrected Navmesh issues and clipping.	
RS Children: Fixed Verna's Blackface bug when using this mod.	
Moonpath to Elsweyr: malerakisvoice voice folder renamed to DBM_RakisVoice to correct missing audio.	
Cell/Location Fixes & Bash Tag updates for all Armory related patches that required it.

6.1.7

Fixed 3D Trees FOMOD Detection.	
Artificer - Artifacts of Skyrim: Corrected patch.	
Dawn of Skyrim: Removed the rock blocking the museum entrance.	
JK's Skyrim: Fixed some clipping issues.	
SkyTest Redux: Updated Slaughterfish DeathItem LL to correct error.	
ADDED: Artificer - An Artifact Overhaul patch	
ADDED: Zim's Immersive Artifacts patch. (Allows Zim's versions of relics to be displayed)
(If you are using ZIM's a NEW GAME is required for this feature to work)

6.1.6

Fixed upload corruption.


6.1.5

More Location/Cell data fixes and updated Bash Tags etc.
Fixed Apophysis selection
GCoN - Removed Airship Guide & Repositioned 3 books in library

6.1.4

Fixed a floating dig site in Clockwork's mortal layer
Fixed an off by 1 error in the hearthfire doll activators
Updated ESO imports so that their containers now take the treasure hunter perks into consideration.

6.1.3

FOMOD update to fix detection. No patch updates.

6.1.2

Moved the dagger from Artifacts of Boethiah so that it no longer overlaps Alessandra's dagger.

6.1.1

More Location/Cell data fixes
Small changes to more patches.
Actually added the Volkihar Knight - Vampire Armor patch  for real.


6.1

Updated Location/Cell data on many patches.
Far too many changes to existing patches to list here.  Just download it, lol.

ADDED: Humanoid Dragon Priests patch - Compatibility for Vahlok the Jailor.
ADDED: Volkihar Knight - Vampire Armor patch
REMOVED: Dev Aveza patch.

6.0.1
Additional updates for v6
Added Dwemer Pipework Reworked

6.0
Too many changes to list. 
In broad strokes: removed previous airship stops, updated patches for v6, removed no longer supported or necessary patches.
