Scriptname DBM_YUMCheeseStorageCrateScript extends ObjectReference  

Potion M
ObjectReference O
ObjectReference L

FormList Property DisplayList Auto

Event OnActivate(ObjectReference akActionRef)
	BlockActivation()
	Utility.Wait(2.0)
	ResetDisplays()
	UpdateDisplay()
EndEvent


Function UpdateDisplay()
	int Index = DisplayList.GetSize()
	While index
			Index -= 1
			O = DisplayList.GetAt(index) as Objectreference
			M = O.GetBaseObject() as Potion
			if Self.GetItemCount(M) >= 1
				O.Enable()
				int numLinkedRefs = O.countLinkedRefChain()
				int Index2 = 1

			While index2 < NumLinkedRefs
				L = O.GetNthLinkedRef(Index2)
					if Self.GetItemCount(M) > Index2
						L.Enable()
					Endif
				Index2 += 1
			EndWhile
		Endif
	EndWhile
	BlockActivation(False)
EndFunction

Function ResetDisplays()
	int ItmCount = DisplayList.GetSize()
	int Index = ItmCount
	While index
			Index -= 1
			O = DisplayList.GetAt(index) as Objectreference
			O.Disable()

			int numLinkedRefs = O.countLinkedRefChain()
			int Index2 = 1

			While index2 < NumLinkedRefs
				L = O.GetNthLinkedRef(Index2)
				L.Disable()
				Index2 += 1
			EndWhile
	EndWhile
EndFunction

